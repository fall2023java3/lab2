package vehicles;

public class Bicycle {
    // Private fields
    private String manufacturer;
    private int numberGears;
    private double maxSpeed;

    // Constructor
    public Bicycle(String manufacturer, int numberGears, double maxSpeed) {
        this.manufacturer = manufacturer;
        this.numberGears = numberGears;
        this.maxSpeed = maxSpeed;
    }

    // Getters for all fields
    public String getManufacturer() {
        return this.manufacturer;
    }

    public int getNumberGears() {
        return this.numberGears;
    }

    public double getMaxSpeed() {
        return this.maxSpeed;
    }

    // Override toString method
    @Override
    public String toString() {
        return "Manufacturer: " + this.manufacturer + ", Number of Gears: " + this.numberGears + ", MaxSpeed: "
                + this.maxSpeed;
    }
}