package application;

import vehicles.Bicycle; // Import the Bicycle class from the "vehicles" package
//My name is Gabriel Benabdallah

public class BikeStore {
    public static void main(String[] args) {
        // Create an array to hold 4 Bicycle objects
        Bicycle[] bikes = new Bicycle[4];

        // Initialize the array with Bicycle objects (hardcoded values)
        bikes[0] = new Bicycle("Specialized", 21, 40.0);
        bikes[1] = new Bicycle("Trek", 18, 35.5);
        bikes[2] = new Bicycle("Giant", 24, 45.2);
        bikes[3] = new Bicycle("Cannondale", 20, 42.3);

        // Loop through the array and print Bicycle details using toString method
        for (Bicycle bike : bikes) {
            System.out.println(bike.toString());
        }
    }
}